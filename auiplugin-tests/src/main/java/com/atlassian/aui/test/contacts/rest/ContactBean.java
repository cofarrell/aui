package com.atlassian.aui.test.contacts.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContactBean
{


    @XmlElement (name = "id")
    private Long id;
    @XmlElement (name = "name")
    private String name;
    @XmlElement (name = "group")
    private String group;
    @XmlElement (name = "number")
    private String number;

    private ContactBean() {}

    public ContactBean(final String name, final String group, final String number)
    {
        this.name = name;
        this.group = group;
        this.number = number;
    }

    public ContactBean(final Long id, final String name, final String group, final String number)
    {
        this.id = id;
        this.name = name;
        this.group = group;
        this.number = number;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getGroup()
    {
        return group;
    }

    public String getNumber()
    {
        return number;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        ContactBean that = (ContactBean) o;

        if (id != that.id) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }
}
