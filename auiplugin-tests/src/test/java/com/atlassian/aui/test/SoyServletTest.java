package com.atlassian.aui.test;

import junit.framework.TestCase;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class SoyServletTest extends TestCase
{

    public void testPathReplacements()
    {
        assertEquals("example", SoyServlet.pathToSoyTemplate("/example"));
        assertEquals("example", SoyServlet.pathToSoyTemplate("/example.soy"));
        assertEquals("examplePath", SoyServlet.pathToSoyTemplate("/example-path.soy"));
        assertEquals("examplePath", SoyServlet.pathToSoyTemplate("/example--path.soy"));
        assertEquals("example.path", SoyServlet.pathToSoyTemplate("/example-/path.soy"));
        assertEquals("example.pathAgain", SoyServlet.pathToSoyTemplate("/example/path-again.soy"));
        assertEquals("testPages.experimental.pageLayout.index", SoyServlet.pathToSoyTemplate("/test-pages/experimental/page-layout/index.soy"));
        assertEquals("testPages.experimental.pageLayout.index", SoyServlet.pathToSoyTemplate("/test-pages/experimental/page-layout/index"));
        assertEquals("testPages.foo.soy.pageLayout.index", SoyServlet.pathToSoyTemplate("/test-pages/foo.soy/page-layout/index.soy"));
    }
}
