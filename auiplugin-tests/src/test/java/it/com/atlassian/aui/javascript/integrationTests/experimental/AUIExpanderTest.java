package it.com.atlassian.aui.javascript.integrationTests.experimental;

import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.ExpanderTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUIExpanderTest extends AbstractAuiIntegrationTest
{

    private ExpanderTestPage expanderTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setUp() {
        expanderTestPage = product.getTestedProduct().visit(ExpanderTestPage.class);
        elementFinder = expanderTestPage.getElementFinder();
    }

    @Test
    public void testClickingTriggerOpensExpander() {
        WebDriverElement trigger = (WebDriverElement) elementFinder.find(By.id("normal-expander-trigger"));
        WebDriverElement content = (WebDriverElement) elementFinder.find(By.id("normal-expander-content"));

        isExpanderNotShown(content);
        trigger.click();
        isExpanderVisible(content);

    }

    @Test
    public void testInitiallyExpandedExpander() {
        WebDriverElement trigger = (WebDriverElement) elementFinder.find(By.id("initially-expanded-expander-trigger"));
        WebDriverElement content = (WebDriverElement) elementFinder.find(By.id("initially-expanded-expander-content"));

        isExpanderVisible(content);
        trigger.click();
        isExpanderHidden(content);
    }

    @Test
    public void testNonCollapsibleExpander() {
        WebDriverElement trigger = (WebDriverElement) elementFinder.find(By.id("continuous-expander-trigger"));
        WebDriverElement content = (WebDriverElement) elementFinder.find(By.id("continuous-expander-content"));

        isExpanderNotShown(content);
        trigger.click();
        trigger.click();
        isExpanderVisible(content);
        isExpanderExpanded(content);
    }

    @Test
    public void testCollapsedToMinHeight() {
        WebDriverElement trigger = (WebDriverElement) elementFinder.find(By.id("min-height-expander-trigger"));
        WebDriverElement content = (WebDriverElement) elementFinder.find(By.id("min-height-expander-content"));

        isExpanderVisible(content);
        assertTrue("Expander should not have a height of 0", content.asWebElement().getSize().getHeight() > 0);
        trigger.click();
        isExpanderVisible(content);
    }

    private void isExpanderVisible(WebDriverElement expanderContent){
        assertTrue("Expander should not be hidden", expanderContent.hasAttribute("aria-hidden", "false") || expanderContent.getAttribute("aria-hidden")==null);
        assertFalse("Expander should not have a height of 0", expanderContent.asWebElement().getSize().getHeight() == 0);
    }

    private void isExpanderHidden(WebDriverElement expanderContent){
        assertTrue("Expander should be hidden", expanderContent.hasAttribute("aria-hidden", "true"));
        assertTrue("Expander should not be expanded", expanderContent.hasAttribute("aria-expanded", "false"));
        isExpanderNotShown(expanderContent);

    }

    private void isExpanderExpanded(WebDriverElement expanderContent){
        assertTrue("Expander should be expanded", expanderContent.hasAttribute("aria-expanded", "true"));
    }

    private void isExpanderNotShown(WebDriverElement expanderContent){
        assertFalse("Expander should have a height of 0", expanderContent.asWebElement().getSize().getHeight() != 0);
    }
}
