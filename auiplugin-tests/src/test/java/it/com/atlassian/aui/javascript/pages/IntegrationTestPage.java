package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * @since 4.0
 */
public class IntegrationTestPage extends TestPage
{
    @ElementBy (id = "context-path")
    PageElement contextPath;

    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/integration/integration-test.html";
    }

    public String getContextPath()
    {
        return contextPath.getText();
    }
}
