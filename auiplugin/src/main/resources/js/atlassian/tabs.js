(function (){
    var REGEX = /#.*/,
        ACTIVE_TAB = "active-tab",
        ACTIVE_PANE = "active-pane",
        ariaSelected = "aria-selected",
        ariaHidden = "aria-hidden";

    AJS.tabs = {
        setup: function () {
            var $tabs = AJS.$(".aui-tabs:not(.aui-tabs-disabled)");

            // Non-menu ARIA setup
            $tabs.attr("role","application");
            $tabs.find(".tabs-pane").each( function() {
                var thisPane = AJS.$(this);
                thisPane.attr("role","tabpanel");
                if (thisPane.hasClass(ACTIVE_PANE)) {
                    thisPane.attr(ariaHidden,"false");
                } else {
                    thisPane.attr(ariaHidden,"true");
                }
            });

            // Menu setup
            for (var i=0, ii = $tabs.length; i < ii; i++) {
                var $tab = $tabs.eq(i);
                if (!$tab.data("aui-tab-events-bound")) {
                    var $tabMenu = $tab.children("ul.tabs-menu");

                    // ARIA setup
                    $tabMenu.attr("role","tablist");
                    $tabMenu.children("li").attr("role","presentation"); // ignore the LIs so tab count is announced correctly
                    $tabMenu.find("> .menu-item a").each( function(){
                        var thisLink = AJS.$(this);
                        AJS._addID(thisLink); // ensure there's an id for later
                        thisLink.attr("role","tab");
                        var targetPane = thisLink.attr("href"); // remember href includes # for selector
                        AJS.$(targetPane).attr("aria-labelledby", thisLink.attr("id"));

                        if (thisLink.parent().hasClass(ACTIVE_TAB)) {
                            thisLink.attr(ariaSelected,"true");
                        } else {
                            thisLink.attr(ariaSelected,"false");
                        }

                    });

                    // Set up click event for tabs
                    $tabMenu.delegate("a", "click", function (e) {
                        AJS.tabs.change(AJS.$(this), e);
                        e && e.preventDefault();
                    });
                    $tab.data("aui-tab-events-bound", true);
                }
            }

            // Vertical tab truncation setup (adds title if clipped)
            AJS.$(".aui-tabs.vertical-tabs").find("a").each( function(i) {
                var thisTab = AJS.$(this);
                // don't override existing titles
                if ( !thisTab.attr("title") ) {
                    var strong = thisTab.children("strong:first");
                    // if text has been truncated, add title
                    if ( AJS.isClipped(strong) ) {
                        thisTab.attr("title", thisTab.text());
                    }
                }
            });
        },
        change: function ($a, e) {
            var $pane = AJS.$($a.attr("href").match(REGEX)[0]);
            $pane.addClass(ACTIVE_PANE).attr(ariaHidden,"false")
                .siblings(".tabs-pane").removeClass(ACTIVE_PANE).attr(ariaHidden,"true");
            $a.parent("li.menu-item").addClass(ACTIVE_TAB)
                .siblings(".menu-item").removeClass(ACTIVE_TAB);
            $a.closest(".tabs-menu").find("a").attr(ariaSelected,"false");
            $a.attr(ariaSelected,"true");
            $a.trigger("tabSelect", {
                tab: $a,
                pane: $pane
            });
        }
    };
    AJS.$(AJS.tabs.setup);
})();
