define("TogglePanelCtrl", ["MainModule"], function(sandboxModule) {

    sandboxModule.controller("TogglePanelCtrl", TogglePanelCtrl);

    function TogglePanelCtrl($scope) {
        $scope.panels = [
            {
                id: 'library',
                title: 'Library',
                displayed: true
            },
            {
                id: 'html',
                title: 'HTML',
                displayed: true,
                left: "0",
                right: "25%"
            },
            {
                id: 'js',
                title: 'Javascript',
                displayed: true,
                left: "25%",
                right: "50%"
            },
            {
                id: 'css',
                title: 'CSS',
                displayed: false,
                left: '50%',
                right: "75%"
            },
            {
                id: 'output',
                title: 'Preview',
                displayed: true,
                left: "75%",
                right: "100%"
            }
        ];

        var setStretchWidth = function() {
            //visible stretch containters
            var visibleStretchPanels = $scope.getVisiblePanels();
            var numVisibleStretchPanels = visibleStretchPanels.length;
            var $contentArea = AJS.$("#sandbox-content-area");
            var $library = AJS.$("#library-nav");
            //the library doesn't count as a stretch panel
            if($scope.getPanelById("library").displayed){
                numVisibleStretchPanels--;
                //make sure nav bar is aligned with content panels
                $contentArea.css("left", $library.outerWidth());
            } else {
                $contentArea.css("left", 0);

            }
            var panelWidths = Math.floor(100 / numVisibleStretchPanels);
            var panelIterator = 0; //can't use index because library will throw it off
            _.each(visibleStretchPanels, function(panel, index){
                if(panel.id != 'library') {
                   panel.left = (panelWidths * panelIterator) + "%";
                   //need to floor the calc so it there's no gap on the right for values like 33%
                   panel.right = 100 - (panelWidths * (panelIterator + 1)) + "%";
                   panelIterator ++;
                }
            });

            //always set the right most panel to 0 so there's no gap
            visibleStretchPanels[visibleStretchPanels.length-1].right = 0;
        };

        $scope.getPanelById = function(panelId) {
            return _.find($scope.panels, function(panel) {
                return panel.id === panelId;
            });
        };

        $scope.togglePanel = function(panelId) {
            var panel = $scope.getPanelById(panelId);
            panel.displayed = !panel.displayed;
            setStretchWidth();
            _.defer(function() {
                _.each($scope.editors, function(editor) {
                    editor.resize();
                });
            });
            if(localStorage){
                localStorage.setItem("toggledStretchPanels", JSON.stringify($scope.panels));
            }
        };

        $scope.isLibraryVisible = function(){
            return $scope.getPanelById('library').displayed;
        };

        $scope.getVisiblePanels = function(){
            return _.filter($scope.panels, function(panel) {
                return panel.displayed;
            });
        };

        AJS.$(window).resize(function(e){
            setStretchWidth();
            _.each($scope.editors, function(editor) {
                editor.resize();
            });
        });

        setStretchWidth();

        if(localStorage){ //feature checking for local storage
            //remove old legacy 'toggles' object from local storage
            if(localStorage.getItem("toggles")){
                localStorage.removeItem("toggles");
            }
            var lstoggles = localStorage.getItem("toggledStretchPanels");
            if(lstoggles === undefined || lstoggles === null) {
                var store = JSON.stringify($scope.panels);
                localStorage.setItem("toggledStretchPanels", store);
            } else {
                $scope.panels = JSON.parse(lstoggles);
                setStretchWidth();
            }
        }
    }
});
