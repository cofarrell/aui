define("Template", ['MainModule'], function() {
    SANDBOX.Library = {
        components: {},
        patterns: {},
        get: function(library, name) {
        	var component = this[library][name] = this[library][name] || {};
        	component["js"] = component["js"] || "";
        	component["html"] = component["html"] || "";
        	return component;
        },
        register: function(name, type, data, library) {
        	var component = this.get(library, name);
        	component[type] = data;
        },

        find: function(name){
            return this.components[name] || this.patterns[name] || undefined; 
        }
    }
});
