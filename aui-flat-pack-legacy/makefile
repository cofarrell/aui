# ------------------------------------------------------------------------------
# Set up all the things.
# ------------------------------------------------------------------------------

# main resources location
RES = ../auiplugin/src/main/resources/
# topper
TOP = top.txt
# core
JS = ${RES}js/atlassian
CSS = ${RES}css/atlassian
# external
EXTJS = ${RES}js/external
# experimental
EXPJS = ${RES}experimental/js/atlassian
EXPCSS = ${RES}experimental/css/atlassian

# output directories
TARGETDIR = target/
TARGET = target/aui/
TESTOUTPUT = unzipped

# flat pack version and filename
DATESTRING := `date +"%Y%m%d"`
TIMESTAMP := `date +'%Y.%m.%d-%H:%M:%S'`
V ?= ${DATESTRING}
FP = aui-flatpack-${V}
# build the regex for later, remember make escaping here for regex escaping there
VERSIONREGEX := s/\$$\{project\.version\}/${V}/g
TIMEREGEX := s/\$$\{timestamp\}/${TIMESTAMP}/g

# CSS and JS includes are abtracted ready to use these in different targets later

# CSS
AUICSS = ${TOP} ${CSS}/html5.css ${CSS}/basic.css ${CSS}/dialog.css ${CSS}/dropdown.css \
         ${CSS}/dropdown2.css ${CSS}/forms.css ${CSS}/icons.css ${CSS}/inline-dialog.css \
         ${CSS}/messages.css ${CSS}/tables.css ${CSS}/tabs.css ${CSS}/toolbar.css
# Untargeted IE CSS - really, means 8 and down
AUIIECSS = ${TOP} ${CSS}/dialog-ie.css ${CSS}/dropdown-ie.css ${CSS}/forms-ie.css ${CSS}/icons-ie.css \
           ${CSS}/inline-dialog-ie.css ${CSS}/tabs-ie.css ${CSS}/toolbar-ie.css
# IE9 targeted CSS
AUIIE9CSS = ${TOP} ${CSS}/dropdown-ie9.css
# Experimental components CSS
AUIEXPCSS = ${TOP} ${EXPCSS}/aui-page-layout.css ${EXPCSS}/aui-header.css ${EXPCSS}/aui-page-typography.css \
            ${EXPCSS}/aui-navigation.css ${EXPCSS}/aui-module.css ${EXPCSS}/aui-buttons.css \
			${EXPCSS}/aui-lozenge.css ${EXPCSS}/aui-badge.css ${EXPCSS}/aui-avatars.css ${EXPCSS}/aui-iconfont.css

# Because we still have some older uglification going on and need to insert semicolons
SAFE = ./separator.js

# JS - minified dependencies NOTE: Raphael has an error in minified version; need to upgrade Raphael
AUIDEPENDENCIESJS = ${TOP} ${EXTJS}/jquery/jquery.js ${SAFE} ${EXTJS}/raphael/raphael.js ${SAFE} ${EXTJS}/jquery/jquery-ui.js
AUIJS = ${TOP} ${EXTJS}/jquery/jquery-compatibility.js ${EXTJS}/jquery/plugins/jquery.form.js \
        ${EXTJS}/jquery/plugins/jquery.aop.js ${JS}/atlassian.js ${JS}/containdropdown.js \
        ${JS}/context-path.cp.js ${JS}/cookie.js ${JS}/dialog.js  ${JS}/raphael/raphael.shadow.js \
        ${JS}/jquery/jquery.os.js ${EXTJS}/jquery/jquery-ui-1.7-bug-fixes.js ${EXTJS}/jquery/jquery-ui-1.7-bug-fixes.js \
        ${JS}/jquery/jquery.hotkeys.js ${JS}/jquery/jquery.moveto.js ${JS}/dropdown.js ${JS}/dropdown2.js ${JS}/event.js \
        ${JS}/firebug.js ${JS}/forms.js ${JS}/icons.js ${JS}/inline-dialog.js ${JS}/messages.js ${JS}/tables.js ${JS}/tabs.js \
        ${JS}/template.js ${JS}/toolbar.js ${JS}/whenitype.js

# ------------------------------------------------------------------------------
# Default AUI flat pack target
# ------------------------------------------------------------------------------
aui: 
	@echo 

# Clean up old copy
	@if test -d ${TARGETDIR}; then echo "Deleting existing copy" ; fi
	@if test -d ${TARGETDIR}; then rm -r ${TARGETDIR} ; fi

# Set up new copy
	@echo "Setting up directories"
	@mkdir -p ${TARGET}css
	@mkdir -p ${TARGET}css/images
	@mkdir -p ${TARGET}js

# CSS
	@echo "Concatenating CSS"
	@cat  ${AUICSS} > ${TARGET}/css/aui.css
	@echo "Concatenating IE CSS"
	@cat  ${AUIIECSS} > ${TARGET}/css/aui-ie.css
	@echo "Concatenating IE9 CSS"
	@cat ${AUIIE9CSS} > ${TARGET}/css/aui-ie9.css
	@echo "Concatenating Experimental CSS"
	@cat ${AUIEXPCSS} > ${TARGET}/css/aui-experimental.css
# JS
	@echo "Concatenating dependency JS"
	@cat ${AUIDEPENDENCIESJS} > ${TARGET}/js/aui-dependencies.js
	@echo "Concatenating AUI JS"
	@cat ${AUIJS} > ${TARGET}/js/aui.js

# Copy files
	@echo "Copying images for CSS"
	@cp -R ${CSS}/images target/aui/css
	@cp -R ${EXPCSS}/images target/aui/css
	@echo "Copying example files"
	@cp -R flatpack-extras/* target/aui

# Set version wherever ${project.version} appears
# Set timestamp wherever ${timestamp} appears
	@./version.sh ${TARGETDIR} ${V} ${VERSIONREGEX} ${TIMESTAMP} ${TIMEREGEX}

# zip it all up
	@echo "Making the zip"
	@cd ${TARGETDIR}; zip -r -q ${FP}.zip aui

# test unzipping
	@echo Unzip a copy for testing \(see ${TARGETDIR}${TESTOUTPUT}\)
	@cd ${TARGETDIR}; unzip -q ${FP}.zip -d ${TESTOUTPUT}

# have a beer
	@echo 
	@echo "Flat pack created! Unzip ${TARGETDIR}${FP}.zip wherever you want to use it."
	@echo "...or, open ${TARGETDIR}aui/aui.html to muck around with it right here!"

# get back to work
	@echo 
	@echo "TO DO LIST"
	@echo "to do: provide optional JS/CSS minification"
	@echo 
